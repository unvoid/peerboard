<?php

namespace PeerboardBundle\Service;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PeercoinManager {

    private $container;
    private $cache;
    private $CURLManager;
    private $daemonPath;

    private $daemonGetInfoResult;

    public function __construct(ContainerInterface $container, CURLManager $CURLManager) {

        $this->container = $container;
        $this->cache = $container->get('cache');
        $this->CURLManager = $CURLManager;
        $this->setDaemonPath($container->getParameter('peercoin_daemon_path'));

    }

    public function setDaemonPath($path) {

        if(!is_string($path)) {

            throw new Exception("Parameter isn't string type.");

        }

//        if(!file_exists($path)) {
//
//            throw new Exception("File in provided path doesn't exists.");
//
//        }

        $this->daemonPath = $path;

        return $this;

    }

    /**
     * @return bool|string
     */
    public function getBlockHeight() {

        if($this->daemonGetInfoResult) {

            if(isset($this->daemonGetInfoResult['blocks'])) {

                return $this->daemonGetInfoResult['blocks'];

            }

        }

        $result = shell_exec("{$this->daemonPath} getinfo");

        if(!$result) {

            return false;

        }

        $info = json_decode($result, true);

        if(!$info || !isset($info['blocks'])) {

            return false;

        }

        $this->daemonGetInfoResult = $info;

        return $info['blocks'];

    }

    /**
     * @return bool|string
     */
    public function getBlockTransactions($blockId) {

        $result = shell_exec("{$this->daemonPath} getblockhash {$blockId}");

        if(!$result) {

            return false;

        }

        $result = shell_exec("{$this->daemonPath} getblock {$result}");

        if(!$result) {

            return false;

        }

        $block = json_decode($result, true);

        if(!$block || !isset($block['tx'])) {

            return false;

        }

        return count($block['tx']);

    }

    /**
     * @return bool|string
     */
    public function getConnectedPeersCount() {

        $result = shell_exec("{$this->daemonPath} getconnectioncount");

        if(!$result) {

            return false;

        }

        if(!is_numeric(trim($result))) {

            return false;

        }

        $result = trim($result);

        return $result;

    }

    /**
     * @return bool|string
     */
    public function getPOSDifficulty() {

        $result = shell_exec("{$this->daemonPath} getdifficulty");

        if(!$result) {

            return false;

        }

        $difficutly = json_decode($result, true);

        if(!$difficutly || !isset($difficutly['proof-of-stake'])) {

            return false;

        }

        $POSDiff = number_format($difficutly['proof-of-stake'], 2, '.', ',');

        return $POSDiff;

    }

    /**
     * @return bool|string
     */
    public function getBalance($fromCache = true) {

        $allowBalance = $this->container->getParameter('peercoin_show_wallet_balance');

        if(!$allowBalance) {

            return false;

        }

        if($fromCache && $this->cache->contains(__METHOD__)) {

            return $this->cache->fetch(__METHOD__);

        }

        $result = shell_exec("{$this->daemonPath} getbalance");

        if(!$result) {

            return false;

        }

        if(!is_numeric(trim($result))) {

            return false;

        }

        $result = trim($result);

        $result = number_format($result, 2, '.', ',');

        if(strlen($result) > 6) {

            $result = str_replace(',', '', $result);

            $result = number_format($result, 0, '.', ',');

        }

        $this->cache->save(__METHOD__, $result, 600);

        return $result;

    }

    /**
     * @return bool|string
     */
    public function getVersion() {

        if($this->daemonGetInfoResult) {

            if(isset($this->daemonGetInfoResult['version'])) {

                return $this->daemonGetInfoResult['version'];

            } else {

                return false;

            }

        }

        $result = shell_exec("{$this->daemonPath} getinfo");

        if(!$result) {

            return false;

        }

        $info = json_decode($result, true);

        if(!$info || !isset($info['version'])) {

            return false;

        }

        $this->daemonGetInfoResult = $info;

        return $info['version'];

    }

    /**
     * @return bool|string
     */
    public function getNewMint() {

        $result = '';

        if($this->daemonGetInfoResult) {

            if(isset($this->daemonGetInfoResult['newmint'])) {

                $result = json_encode($this->daemonGetInfoResult);

            }

        }

        if(!$result) {

            $result = shell_exec("{$this->daemonPath} getinfo");

        }

        if(!$result) {

            return false;

        }

        $info = json_decode($result, true);

        if(!$info || !isset($info['newmint'])) {

            return false;

        }

        $info['newmint'] = trim($info['newmint']);

        $info['newmint'] = str_replace(',', '.', $info['newmint']);

        if(strlen($info['newmint']) > 6) {

            $info['newmint'] = number_format($info['newmint'], 0, '.', ',');

        } else {

            $info['newmint'] = number_format($info['newmint'], 2, '.', ',');

        }

        $this->daemonGetInfoResult = $info;

        return $info['newmint'];

    }

    /**
     * @return bool|string
     */
    public function getStake() {

        $result = '';

        if($this->daemonGetInfoResult) {

            if(isset($this->daemonGetInfoResult['stake'])) {

                $result = json_encode($this->daemonGetInfoResult);

            }

        }

        if(!$result) {

            $result = shell_exec("{$this->daemonPath} getinfo");

        }

        if(!$result) {

            return false;

        }

        $info = json_decode($result, true);

        if(!$info || !isset($info['stake'])) {

            return false;

        }

        $info['stake'] = trim($info['stake']);

        $info['stake'] = str_replace(',', '.', $info['stake']);

        if(strlen($info['stake']) > 6) {

            $info['stake'] = number_format($info['stake'], 0, '.', ',');

        } else {

            $info['stake'] = number_format($info['stake'], 2, '.', ',');

        }

        $this->daemonGetInfoResult = $info;

        return $info['stake'];

    }

    /**
     * @return bool|string
     */
    public function getPeerInfo($fromCache = true) {

        if($fromCache && $this->cache->contains(__METHOD__)) {

            return unserialize($this->cache->fetch(__METHOD__));

        }

        $result = shell_exec("{$this->daemonPath} getpeerinfo");

        if(!$result) {

            return false;

        }

        $peerInfo = json_decode($result, true);

        if(!$peerInfo || !is_array($peerInfo)) {

            return false;

        }

        $this->cache->save(__METHOD__, serialize($peerInfo), 30);

        return $peerInfo;

    }

    /**
     * @return bool|string
     */
    public function getNewAndOldConnectedPeers() {

        $peerInfo = $this->getPeerInfo();

        // disconnected from daemon
        if(!$peerInfo) {

            if($this->cache->contains(__METHOD__)) {

                $cachedPeerInfo = unserialize($this->cache->fetch(__METHOD__));
                $this->cache->delete(__METHOD__);
                return ['new' => [], 'old' => $cachedPeerInfo];

            }

            return false;

        }

        $cachedPeerInfo = [];

        if($this->cache->contains(__METHOD__)) {

            $cachedPeerInfo = unserialize($this->cache->fetch(__METHOD__));

        }

        if(!count($cachedPeerInfo)) {

            $cachedPeerInfo = $peerInfo;

            $this->cache->save(__METHOD__, serialize($cachedPeerInfo), 3600 * 12);

            return ['new' => $cachedPeerInfo, 'old' => []];

        }

        // compare; add new peers to cache, discard old from cache

        $result = [
            'new' => [],
            'old' => [],
        ];

        $currentHashes = [];
        $oldHashes = [];

        foreach($peerInfo as $key => $info) {

            $currentHashes[$key] = sha1($info['addr'] . $info['subver']);

        }

        foreach($cachedPeerInfo as $key => $info) {

            $oldHashes[$key] = sha1($info['addr'] . $info['subver']);

        }

        foreach($oldHashes as $key => $hash) {

            if(!in_array($hash,$currentHashes)) {

                // old peer found
                $result['old'][] = $cachedPeerInfo[$key];

            }

        }

        foreach($currentHashes as $key => $hash) {


            if(!in_array($hash,$oldHashes)) {

                // new peer found
                $result['new'][] = $peerInfo[$key];

            }

        }

        if(!count($result['new']) && !count($result['old'])) {

            return false;

        }

        $cachedPeerInfo = $peerInfo;

        $this->cache->save(__METHOD__, serialize($cachedPeerInfo), 3600 * 12);

        return $result;

    }

    /**
     * @return bool|string
     */
    public function getPrice($fromCache = true) {

        if($fromCache && $this->cache->contains(__METHOD__)) {

            return $this->cache->fetch(__METHOD__);

        }

        $result = $this->CURLManager->getURL('http://coinmarketcap-nexuist.rhcloud.com/api/ppc');

        if(!$result) {

            return false;

        }

        $array = json_decode($result, true);

        if(!$array || !isset($array['price']['usd'])) {

            return false;

        }

        if(!is_numeric($array['price']['usd'])) {

            return false;

        }

        $price = $array['price']['usd'];

        $price = number_format($price,2,'.',',');

        $this->cache->save(__METHOD__, $price, 1800);

        return $price;

    }

    /**
     * @return bool|string
     */
    public function  getRecentForumPost() {

        if($this->cache->contains(__METHOD__)) {

            return unserialize($this->cache->fetch(__METHOD__));

        }

        $simpleHTMLDOMParser = new \PeerboardBundle\Library\simple_html_dom();

        $peercointalk = $this->CURLManager->getURL('https://www.peercointalk.org');

        if(!$peercointalk) {

            return false;

        }

        if(!$simpleHTMLDOMParser->load($peercointalk)) {

            return false;

        }

        $recentPostHTML = $simpleHTMLDOMParser->find('//*[@id="ic_recentposts"]/dt[1]')[0];

        if(!$recentPostHTML || !is_object($recentPostHTML)) {

            return false;

        }

        $recentPostHTML = $recentPostHTML->find('a');

        if(!$recentPostHTML || !is_array($recentPostHTML) || !count($recentPostHTML)) {

            return false;

        }

        $recentPostHTML = $recentPostHTML[0];

        if(!$recentPostHTML || !is_object($recentPostHTML)) {

            return false;

        }

        $array = [$recentPostHTML->innertext,$recentPostHTML->href];

        $this->cache->save(__METHOD__, serialize($array), 1800);

        return $array;

    }

    /**
     * @return bool
     */
    public function isUnlockedForMinting() {

        if($this->cache->contains(__METHOD__)) {

            return $this->cache->fetch(__METHOD__);

        }

        if($this->daemonGetInfoResult) {

            if(isset($this->daemonGetInfoResult['unlocked_until'])) {

                if($this->daemonGetInfoResult['unlocked_until']) {

                    $unlockedUntil = (new \DateTime())->setTimestamp($this->daemonGetInfoResult['unlocked_until']);
                    $now = new \DateTime();

                    if($unlockedUntil > $now) {

                        $this->cache->save(__METHOD__, true, 30);

                        return true;

                    }

                } else {

                    $this->cache->save(__METHOD__, false, 30);

                    return false;

                }

            } else {

                $this->cache->save(__METHOD__, false, 30);

                return false;

            }

        }

        $result = shell_exec("{$this->daemonPath} getinfo");

        if(!$result) {

            $this->cache->save(__METHOD__, false, 30);

            return false;

        }

        $info = json_decode($result, true);

        if(!$info || !isset($info['unlocked_until'])) {

            $this->cache->save(__METHOD__, false, 30);

            return false;

        }

        $this->daemonGetInfoResult = $info;

        $unlockedUntil = (new \DateTime())->setTimestamp($info['unlocked_until']);
        $now = new \DateTime();

        if($unlockedUntil > $now) {

            $this->cache->save(__METHOD__, true, 30);

            return true;

        }

    }

}