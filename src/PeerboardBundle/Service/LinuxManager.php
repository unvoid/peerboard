<?php

namespace PeerboardBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;

class LinuxManager {

    private $container;

    public function __construct(ContainerInterface $container) {

        $this->container = $container;

    }

    /**
     * @return bool|string
     */
    public function getLoad() {

        $result = shell_exec("uptime | awk -F'[a-z]:' '{ print $2}'");

        if(!$result && !trim($result)) {

            return false;

        }

        return $result;

    }

    /**
     * @return bool|string
     */
    public function getUsedMemory() {

        $result = shell_exec("free | awk 'FNR == 3 {print $3/($3+$4)*100}'");

        if(!$result && !trim($result)) {


            return false;

        }

        $result = number_format(trim($result),2,'.',',');

        return $result;

    }

    /**
     * @return bool|string
     */
    public function getCPUTemp() {

        $result = shell_exec("sensors");

        preg_match('~\+(.*?).C\s+\(high~', $result, $temp);

        if($temp && is_array($temp) && count($temp) > 1) {

            return (int)$temp[1];

        }

        return false;

    }

}