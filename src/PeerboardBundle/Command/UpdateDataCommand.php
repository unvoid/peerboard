<?php
namespace PeerboardBundle\Command;

use PeerboardBundle\Entity\DataCPULoad;
use PeerboardBundle\Entity\DataPOSDifficulty;
use PeerboardBundle\Entity\DataUsedMemory;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use PeerboardBundle\Entity\DataTemperature;
use PeerboardBundle\Entity\DataConnection;
use PeerboardBundle\Entity\DataTransactions;


/**
 * Class UpdateDataCommand
 * @package PeerboardBundle\Command
 */
class UpdateDataCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('peerboard:data:update')
            ->setDescription('Updates all data storing it in database.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container          = $this->getContainer();
        $em                 = $container->get('doctrine.orm.entity_manager');
        $peercoinManager    = $container->get('peerboard.peercoinmanager');
        $linuxManager       = $container->get('peerboard.linuxmanager');
        $transactionsRepo   = $em->getRepository('PeerboardBundle:DataTransactions');

        $output->writeln('Starting...');

        $temp               = $linuxManager->getCPUTemp();
        $load               = $linuxManager->getLoad();
        $usedMemory         = $linuxManager->getUsedMemory();
        $connections        = $peercoinManager->getConnectedPeersCount();
        $POSDifficulty      = $peercoinManager->getPOSDifficulty();
        $blockId            = $peercoinManager->getBlockHeight();
        $transactions       = $peercoinManager->getBlockTransactions($blockId);

        if($temp) {

            $output->writeln('<info>Temperature: <comment>' . $temp .'</comment></info>');
            $dataTemperature = new DataTemperature();
            $dataTemperature->setTemperature($temp);
            $em->persist($dataTemperature);

        }

        if($load) {

            $explodedLoad = explode(', ',$load);
            $explodedLoad[0] = str_replace(',', '.', $explodedLoad[0]);

            $output->writeln('<info>Load: <comment>' . trim($explodedLoad[0]) .'</comment></info>');
            $dataCPULoad = new DataCPULoad();
            $dataCPULoad->setCpuLoad($explodedLoad[0]);
            $em->persist($dataCPULoad);

        }

        if($usedMemory) {

            $output->writeln('<info>Used RAM: <comment>' . $usedMemory .'</comment></info>');
            $dataUsedMemory = new DataUsedMemory();
            $dataUsedMemory->setUsedMemory($usedMemory);
            $em->persist($dataUsedMemory);

        }

        if($connections) {

            $output->writeln('<info>Connections: <comment>' . $connections .'</comment></info>');
            $dataConnection = new DataConnection();
            $dataConnection->setConnections($connections);
            $em->persist($dataConnection);

        }

        if($POSDifficulty) {

            $output->writeln('<info>POS Difficulty: <comment>' . $POSDifficulty .'</comment></info>');
            $dataPOSDifficulty = new DataPOSDifficulty();
            $dataPOSDifficulty->setDifficulty($POSDifficulty);
            $em->persist($dataPOSDifficulty);

        }

        if($transactions) {

            $output->writeln('<info>Block: '.$blockId.' transactions: <comment>' . $transactions .'</comment></info>');

            $dbTransaction = $transactionsRepo->findByBlockId($blockId);

            if(!$dbTransaction) {

                $dataTransactions = new DataTransactions();
                $dataTransactions->setTransactions($transactions);
                $dataTransactions->setBlockId($blockId);
                $em->persist($dataTransactions);

                $output->writeln('<info>Block transactions saved</info>');

            } else {

                $output->writeln('<info>Block Transactions already in database. Omitting.</info>');

            }

        }

        $em->flush();

        $output->writeln('DONE');

    }
}