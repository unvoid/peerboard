<?php
namespace PeerboardBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use PeerboardBundle\Entity\DataTransactions;


/**
 * Class UpdateDataBlockTransactionsCommand
 * @package PeerboardBundle\Command
 */
class UpdateDataBlockTransactionsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('peerboard:data:transactions:update')
            ->setDescription('Updates transactions per block storing it in database.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container          = $this->getContainer();
        $em                 = $container->get('doctrine.orm.entity_manager');
        $peercoinManager    = $container->get('peerboard.peercoinmanager');
        $transactionsRepo   = $em->getRepository('PeerboardBundle:DataTransactions');

        $output->writeln('Starting...');

        $blockId            = $peercoinManager->getBlockHeight();
        $transactions       = $peercoinManager->getBlockTransactions($blockId);

        if($transactions) {

            $output->writeln('<info>Block: '.$blockId.' transactions: <comment>' . $transactions .'</comment></info>');

            $dbTransaction = $transactionsRepo->findByBlockId($blockId);

            if(!$dbTransaction) {

                $dataTransactions = new DataTransactions();
                $dataTransactions->setTransactions($transactions);
                $dataTransactions->setBlockId($blockId);
                $em->persist($dataTransactions);

                $output->writeln('<info>Block transactions saved</info>');

            } else {

                $output->writeln('<info>Block Transactions already in database. Omitting.</info>');

            }

        }

        $em->flush();

        $output->writeln('DONE');

    }
}