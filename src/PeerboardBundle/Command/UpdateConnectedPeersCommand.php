<?php
namespace PeerboardBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use PeerboardBundle\Entity\DataTransactions;


/**
 * Class UpdateConnectedPeersCommand
 * @package PeerboardBundle\Command
 */
class UpdateConnectedPeersCommand extends ContainerAwareCommand
{
    const CACHE_KEY = 'peerboard.command.updateconnectedpeers';
    const CACHE_TTL = 55;

    protected function configure()
    {
        $this
            ->setName('peerboard:connectedpeers:update')
            ->setDescription('Updates information about connected/disconnected peers.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $container          = $this->getContainer();
        $cache              = $container->get('cache');
        $peercoinManager    = $container->get('peerboard.peercoinmanager');

        $output->writeln('Starting...');

        $newOldPeers = $peercoinManager->getNewAndOldConnectedPeers();

        if($newOldPeers) {

            $newOldPeersSerialized = serialize($newOldPeers);

            $cache->save(self::CACHE_KEY, $newOldPeersSerialized, self::CACHE_TTL);

        }

        $output->writeln('DONE');

    }
}