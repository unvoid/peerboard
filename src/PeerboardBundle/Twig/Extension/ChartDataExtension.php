<?php

namespace PeerboardBundle\Twig\Extension;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\FilesystemCache;


/**
 * Class ChartDataExtension
 * @package   PeerboardBundle\Twig\Extension
 */
class ChartDataExtension extends \Twig_Extension
{

    const CHART_DATA_CACHE_TTL = 60;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FilesystemCache
     */
    private $cache;

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('chartDataGetTemperature',         [$this, 'getTemperature']),
            new \Twig_SimpleFunction('chartDataGetMaxTemperature',      [$this, 'getMaxTemperature']),
            new \Twig_SimpleFunction('chartDataGetMinTemperature',      [$this, 'getMinTemperature']),
            new \Twig_SimpleFunction('chartDataGetConnections',         [$this, 'getConnections']),
            new \Twig_SimpleFunction('chartDataGetMaxConnections',      [$this, 'getMaxConnections']),
            new \Twig_SimpleFunction('chartDataGetMinConnections',      [$this, 'getMinConnections']),
            new \Twig_SimpleFunction('chartDataGetCPULoad',             [$this, 'getCPULoad']),
            new \Twig_SimpleFunction('chartDataGetMaxCPULoad',          [$this, 'getMaxCPULoad']),
            new \Twig_SimpleFunction('chartDataGetMinCPULoad',          [$this, 'getMinCPULoad']),
            new \Twig_SimpleFunction('chartDataGetUsedMemory',          [$this, 'getUsedMemory']),
            new \Twig_SimpleFunction('chartDataGetMaxUsedMemory',       [$this, 'getMaxUsedMemory']),
            new \Twig_SimpleFunction('chartDataGetMinUsedMemory',       [$this, 'getMinUsedMemory']),
            new \Twig_SimpleFunction('chartDataGetPOSDiff',             [$this, 'getPOSDiff']),
            new \Twig_SimpleFunction('chartDataGetMaxPOSDiff',          [$this, 'getMaxPOSDiff']),
            new \Twig_SimpleFunction('chartDataGetMinPOSDiff',          [$this, 'getMinPOSDiff']),
            new \Twig_SimpleFunction('chartDataGetTransactions',        [$this, 'getTransactions']),
            new \Twig_SimpleFunction('chartDataGetMaxTransactions',     [$this, 'getMaxTransactions']),
            new \Twig_SimpleFunction('chartDataGetMinTransactions',     [$this, 'getMinTransactions']),
        ];
    }

    public function setEntityManager(EntityManager $em) {

        $this->em = $em;

    }

    public function setCache(FilesystemCache $cache) {

        $this->cache = $cache;

    }

    public function getTemperature($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $temperatureRepo = $this->em->getRepository('PeerboardBundle:DataTemperature');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $temperatureRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgTemperatures = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgTemperature = $temperatureRepo->getAvgTemperatureFromDateToDate($fromDate, $toDate);

            if(!$avgTemperature) {

                $avgTemperature = '23'; // if no data in this period assuming home temperature

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgTemperatures[] = number_format($avgTemperature,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgTemperatures) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgTemperatures) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMaxTemperature($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $temperatureRepo = $this->em->getRepository('PeerboardBundle:DataTemperature');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $temperatureRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgTemperatures = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgTemperature = $temperatureRepo->getMaxTemperatureFromDateToDate($fromDate, $toDate);

            if(!$avgTemperature) {

                $avgTemperature = '23'; // if no data in this period assuming home temperature

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgTemperatures[] = number_format($avgTemperature,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgTemperatures) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgTemperatures) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMinTemperature($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $temperatureRepo = $this->em->getRepository('PeerboardBundle:DataTemperature');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $temperatureRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgTemperatures = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgTemperature = $temperatureRepo->getMinTemperatureFromDateToDate($fromDate, $toDate);

            if(!$avgTemperature) {

                $avgTemperature = '23'; // if no data in this period assuming home temperature

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgTemperatures[] = number_format($avgTemperature,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgTemperatures) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgTemperatures) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getCPULoad($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $cpuLoadRepo = $this->em->getRepository('PeerboardBundle:DataCPULoad');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $cpuLoadRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgLoads = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgLoad = $cpuLoadRepo->getAvgCPULoadFromDateToDate($fromDate, $toDate);

            if(!$avgLoad) {

                $avgLoad = '0'; // if no data in this period assuming 0 load

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgLoads[] = number_format($avgLoad,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgLoads) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgLoads) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMaxCPULoad($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $cpuLoadRepo = $this->em->getRepository('PeerboardBundle:DataCPULoad');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $cpuLoadRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgLoads = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgLoad = $cpuLoadRepo->getMaxCPULoadFromDateToDate($fromDate, $toDate);

            if(!$avgLoad) {

                $avgLoad = '0'; // if no data in this period assuming 0 load

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgLoads[] = number_format($avgLoad,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgLoads) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgLoads) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMinCPULoad($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $cpuLoadRepo = $this->em->getRepository('PeerboardBundle:DataCPULoad');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $cpuLoadRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgLoads = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgLoad = $cpuLoadRepo->getMinCPULoadFromDateToDate($fromDate, $toDate);

            if(!$avgLoad) {

                $avgLoad = '0'; // if no data in this period assuming 0 load

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgLoads[] = number_format($avgLoad,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgLoads) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgLoads) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getConnections($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $connectionRepo = $this->em->getRepository('PeerboardBundle:DataConnection');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $connectionRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgConnections = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgConnection = $connectionRepo->getAvgConnectionsFromDateToDate($fromDate, $toDate);

            if(!$avgConnection) {

                $avgConnection = '0'; // if no data in this period assuming 0 connections

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgConnections[] = number_format($avgConnection,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }


            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgConnections) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgConnections) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMaxConnections($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $connectionRepo = $this->em->getRepository('PeerboardBundle:DataConnection');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $connectionRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgConnections = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgConnection = $connectionRepo->getMaxConnectionsFromDateToDate($fromDate, $toDate);

            if(!$avgConnection) {

                $avgConnection = '0'; // if no data in this period assuming 0 connections

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgConnections[] = number_format($avgConnection,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }


            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgConnections) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgConnections) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMinConnections($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $connectionRepo = $this->em->getRepository('PeerboardBundle:DataConnection');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $connectionRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgConnections = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgConnection = $connectionRepo->getMinConnectionsFromDateToDate($fromDate, $toDate);

            if(!$avgConnection) {

                $avgConnection = '0'; // if no data in this period assuming 0 connections

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgConnections[] = number_format($avgConnection,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }


            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgConnections) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgConnections) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getUsedMemory($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $usedMemoryRepo = $this->em->getRepository('PeerboardBundle:DataUsedMemory');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $usedMemoryRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgUsedMemory = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgUsedMem = $usedMemoryRepo->getAvgUsedMemoryFromDateToDate($fromDate, $toDate);

            if(!$avgUsedMem) {

                $avgUsedMem = '0'; // if no data in this period assuming 0% used memory

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgUsedMemory[] = number_format($avgUsedMem,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgUsedMemory) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgUsedMemory) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMaxUsedMemory($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $usedMemoryRepo = $this->em->getRepository('PeerboardBundle:DataUsedMemory');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $usedMemoryRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgUsedMemory = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgUsedMem = $usedMemoryRepo->getMaxUsedMemoryFromDateToDate($fromDate, $toDate);

            if(!$avgUsedMem) {

                $avgUsedMem = '0'; // if no data in this period assuming 0% used memory

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgUsedMemory[] = number_format($avgUsedMem,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgUsedMemory) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgUsedMemory) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMinUsedMemory($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $usedMemoryRepo = $this->em->getRepository('PeerboardBundle:DataUsedMemory');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $usedMemoryRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgUsedMemory = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgUsedMem = $usedMemoryRepo->getMinUsedMemoryFromDateToDate($fromDate, $toDate);

            if(!$avgUsedMem) {

                $avgUsedMem = '0'; // if no data in this period assuming 0% used memory

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgUsedMemory[] = number_format($avgUsedMem,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgUsedMemory) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgUsedMemory) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getPOSDiff($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $diffRepo = $this->em->getRepository('PeerboardBundle:DataPOSDifficulty');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $diffRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgDifficulty = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgDiff = $diffRepo->getAvgDifficultyFromDateToDate($fromDate, $toDate);

            if(!$avgDiff) {

                $avgDiff = '0'; // if no data in this period assuming 0 difficulty

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgDifficulty[] = number_format($avgDiff,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgDifficulty) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgDifficulty) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMaxPOSDiff($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $diffRepo = $this->em->getRepository('PeerboardBundle:DataPOSDifficulty');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $diffRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgDifficulty = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgDiff = $diffRepo->getMaxDifficultyFromDateToDate($fromDate, $toDate);

            if(!$avgDiff) {

                $avgDiff = '0'; // if no data in this period assuming 0 difficulty

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgDifficulty[] = number_format($avgDiff,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgDifficulty) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgDifficulty) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMinPOSDiff($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $diffRepo = $this->em->getRepository('PeerboardBundle:DataPOSDifficulty');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $diffRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgDifficulty = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgDiff = $diffRepo->getMinDifficultyFromDateToDate($fromDate, $toDate);

            if(!$avgDiff) {

                $avgDiff = '0'; // if no data in this period assuming 0 difficulty

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgDifficulty[] = number_format($avgDiff,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgDifficulty) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgDifficulty) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getTransactions($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $transactionsRepo = $this->em->getRepository('PeerboardBundle:DataTransactions');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $transactionsRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgTransactions = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgTrans = $transactionsRepo->getAvgTransactionsFromDateToDate($fromDate, $toDate);

            if(!$avgTrans) {

                $avgTrans = '0'; // if no data in this period assuming 0 transactions

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgTransactions[] = number_format($avgTrans,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgTransactions) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgTransactions) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMaxTransactions($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $transactionsRepo = $this->em->getRepository('PeerboardBundle:DataTransactions');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $transactionsRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgTransactions = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgTrans = $transactionsRepo->getMaxTransactionsFromDateToDate($fromDate, $toDate);

            if(!$avgTrans) {

                $avgTrans = '0'; // if no data in this period assuming 0 transactions

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgTransactions[] = number_format($avgTrans,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgTransactions) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgTransactions) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    public function getMinTransactions($dataFromPastHours = 3, $labelsCount = 24) {

        // trying to load data from cache first

        $cacheKey = __METHOD__ . $dataFromPastHours . $labelsCount;

        if($this->cache->contains($cacheKey)) {

            return unserialize($this->cache->fetch($cacheKey));

        }

        $transactionsRepo = $this->em->getRepository('PeerboardBundle:DataTransactions');

        $maxPeriodMinutes = $dataFromPastHours * 60;
        $singlePointPeriodMinutes = $maxPeriodMinutes / $labelsCount;
        $singlePointPeriodSeconds = round(($singlePointPeriodMinutes - floor($singlePointPeriodMinutes)) * 60, 0, PHP_ROUND_HALF_UP);
        $singlePointPeriodMinutes = floor($singlePointPeriodMinutes);

        $dateHalfMinutes = $singlePointPeriodMinutes / 2;
        $dateHalfSeconds = (round(($dateHalfMinutes - floor($dateHalfMinutes)) * 60, 0, PHP_ROUND_HALF_UP) + $singlePointPeriodSeconds) / 2;
        $dateHalfMinutes = floor($dateHalfMinutes);

        $latestDate = new \DateTime();
        $latestDatabaseEntry = $transactionsRepo->findOneBy([],['createdAt' => 'desc']);

        if($latestDatabaseEntry) {

            $latestDate = $latestDatabaseEntry->getCreatedAt();

        }

        $fromDate = clone $latestDate;
        $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
        $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        $toDate = clone $latestDate;

        $avgTransactions = [];
        $labels = [];

        for($i = 0; $i < $labelsCount; $i++) {

            $avgTrans = $transactionsRepo->getMinTransactionsFromDateToDate($fromDate, $toDate);

            if(!$avgTrans) {

                $avgTrans = '0'; // if no data in this period assuming 0 transactions

            }

            $labelDate = clone $toDate;
            $labelDate->modify('-' . $dateHalfMinutes . ' minutes');
            $labelDate->modify('-' . $dateHalfSeconds . ' seconds');

            $avgTransactions[] = number_format($avgTrans,2,'.',',');

            if($dataFromPastHours > 24) {

                $labels[] = $labelDate->format('m-d H:i');

            } else {

                $labels[] = $labelDate->format('H:i');

            }

            $toDate = clone $fromDate;

            $fromDate = $fromDate->modify('-' . $singlePointPeriodMinutes . ' minutes');
            $fromDate = $fromDate->modify('-' . $singlePointPeriodSeconds . ' seconds');

        }

        $data = [
            'd' => '[]',
            't' => '[]',
        ];

        if(count($avgTransactions) && count($labels)) {

            $data = [
                'd' => '[\'' . implode('\',\'',$avgTransactions) . '\']',
                'l' => '[\'' . implode('\',\'',$labels) . '\']',
            ];

        }

        $this->cache->save($cacheKey,serialize($data),self::CHART_DATA_CACHE_TTL);

        return $data;

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ChartDataExtension';
    }
}
