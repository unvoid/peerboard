<?php

namespace PeerboardBundle\Twig\Extension;
use PeerboardBundle\Service\PeercoinManager;

/**
 * Class PeercoinExtension
 * @package   PeerboardBundle\Twig\Extension
 */
class PeercoinExtension extends \Twig_Extension
{

    /**
     * @var PeercoinManager
     */
    private $peercoinManager;

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('peercoinGetDaemonVersion', [$this, 'getDaemonVersion']),
            new \Twig_SimpleFunction('peercoinIsUnlockedForMinting', [$this, 'isUnlockedForMinting']),
        ];
    }

    public function setPeercoinManager(PeercoinManager $peercoinManager) {

        $this->peercoinManager = $peercoinManager;

    }

    public function getDaemonVersion() {

        $version = $this->peercoinManager->getVersion();

        if($version) {

            return $version;

        }

        return 'N/A';

    }

    public function isUnlockedForMinting() {

        $unlocked = $this->peercoinManager->isUnlockedForMinting();

        if($unlocked) {

            return $unlocked;

        }

        return false;

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'PeercoinExtension';
    }
}
