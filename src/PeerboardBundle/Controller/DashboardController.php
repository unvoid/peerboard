<?php

namespace PeerboardBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PeerboardBundle\Library;

/**
 * Class DashboardController
 * @package PeerboardBundle\Controller
 */
class DashboardController extends ExtendController
{

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request, $chartPeriodHours) {

        $chartPeriodHours = (float)$chartPeriodHours;

        $peercoinManager        = $this->container->get('peerboard.peercoinManager');
        $linuxManager           = $this->container->get('peerboard.linuxManager');
        $temperatureRepo        = $this->em->getRepository('PeerboardBundle:DataTemperature');
        $connectionRepo         = $this->em->getRepository('PeerboardBundle:DataConnection');
        $CPULoadRepo            = $this->em->getRepository('PeerboardBundle:DataCPULoad');
        $usedMemoryRepo         = $this->em->getRepository('PeerboardBundle:DataUsedMemory');
        $POSDiffRepo            = $this->em->getRepository('PeerboardBundle:DataPOSDifficulty');
        $transactionsRepo       = $this->em->getRepository('PeerboardBundle:DataTransactions');

        /*
         * peercoin data
         */

        $peercoinBalance        = $peercoinManager->getBalance();
        $peercoinConnectedPeersCount = $peercoinManager->getConnectedPeersCount();
        $peercoinBlockHeight    = $peercoinManager->getBlockHeight();
        $peercoinBlockTransactions = $peercoinManager->getBlockTransactions($peercoinBlockHeight);
        $peercoinPOSDifficutly  = $peercoinManager->getPOSDifficulty();
        $peercoinPrice          = $peercoinManager->getPrice();
        $peercoinMint           = $peercoinManager->getNewMint();
        $peercoinStake          = $peercoinManager->getStake();

        /*
         * connected peers
         */

        $peercoinConnectedPeers = $peercoinManager->getPeerInfo();

        /*
         * linux data
         */

        $linuxCPUTemp           = $linuxManager->getCPUTemp();
        $linuxUsedMemory        = $linuxManager->getUsedMemory();
        $linuxSysLoad           = $linuxManager->getLoad();

        if($linuxSysLoad) {

            $explodedLoad = explode(', ',$linuxSysLoad);
            $linuxSysLoad = str_replace(',', '.', $explodedLoad[0]);

        }

        /*
         * peercoin avg data
         */

        $linux24hPercentChangeAvhCPULoad            = 0;
        $linux24hPercentChangeAvhUsedMemory         = 0;
        $linux24hPercentChangeAvhCPUTemp            = 0;
        $peercoin24hPercentChangeAvgConnections     = 0;
        $peercoin24hPercentChangeAvgTransactions    = 0;
        $peercoin24hPercentChangeAvgDifficulty      = 0;

        if($linuxSysLoad && $linuxSysLoad !== 'N/A') {

            $linux24hAvgSysLoad = $CPULoadRepo->getLast24hAvgCPULoad();

            if($linux24hAvgSysLoad) {

                $linux24hPercentChangeAvhCPULoad = (($linuxSysLoad / $linux24hAvgSysLoad) * 100) - 100;
                $linux24hPercentChangeAvhCPULoad = number_format($linux24hPercentChangeAvhCPULoad,2,'.',',');

            }

        }

        if($linuxUsedMemory && $linuxUsedMemory !== 'N/A') {

            $linux24hAvgUsedMemory = $usedMemoryRepo->getLast24hAvgUsedMemory();

            if($linux24hAvgUsedMemory) {

                $linux24hPercentChangeAvhUsedMemory = (($linuxUsedMemory / $linux24hAvgUsedMemory) * 100) - 100;
                $linux24hPercentChangeAvhUsedMemory = number_format($linux24hPercentChangeAvhUsedMemory,2,'.',',');

            }


        }

        if($linuxCPUTemp && $linuxCPUTemp !== 'N/A') {

            $linux24hAvgCPUTemp = $temperatureRepo->getLast24hAvgTemperature();

            if($linux24hAvgCPUTemp) {

                $linux24hPercentChangeAvhCPUTemp = (($linuxCPUTemp / $linux24hAvgCPUTemp) * 100) - 100;
                $linux24hPercentChangeAvhCPUTemp = number_format($linux24hPercentChangeAvhCPUTemp,2,'.',',');

            }

        }

        if($peercoinConnectedPeers && $peercoinConnectedPeersCount !== 'N/A') {

            $peercoin24hAvgConnections = $connectionRepo->getLast24hAvgConnections();

            if($peercoin24hAvgConnections) {

                $peercoin24hPercentChangeAvgConnections = (($peercoinConnectedPeersCount / $peercoin24hAvgConnections) * 100) - 100;
                $peercoin24hPercentChangeAvgConnections = number_format($peercoin24hPercentChangeAvgConnections,2,'.',',');

            }

        }

        if($peercoinBlockTransactions && $peercoinBlockTransactions !== 'N/A') {

            $peercoin24hAvgTransactions = $transactionsRepo->getLast24hAvgTransactions();

            if($peercoin24hAvgTransactions) {

                $peercoin24hPercentChangeAvgTransactions = (($peercoinBlockTransactions / $peercoin24hAvgTransactions) * 100) - 100;
                $peercoin24hPercentChangeAvgTransactions = number_format($peercoin24hPercentChangeAvgTransactions,2,'.',',');

            }

        }

        if($peercoinPOSDifficutly && $peercoinPOSDifficutly !== 'N/A') {

            $peercoin24hAvgDifficulty = $POSDiffRepo->getLast24hAvgDifficulty();

            if($peercoin24hAvgDifficulty) {

                $peercoin24hPercentChangeAvgDifficulty = (($peercoinPOSDifficutly / $peercoin24hAvgDifficulty) * 100) - 100;
                $peercoin24hPercentChangeAvgDifficulty = number_format($peercoin24hPercentChangeAvgDifficulty,2,'.',',');

            }

        }

        /*
         * recent post from peercointalk
         */

        $recentPostTitle = '';
        $recentPostHref = '';

        if($recentPost = $peercoinManager->getRecentForumPost()) {

            $recentPostTitle = $recentPost[0];
            $recentPostHref = $recentPost[1];

        }

        return $this->render('@Peerboard/Dashboard/index.html.twig',[
            'peercoinBalance'           => $peercoinBalance ? $peercoinBalance : 'N/A',
            'peercoinConnectedPeersCount' => $peercoinConnectedPeersCount ? $peercoinConnectedPeersCount :  'N/A',
            'peercoinBlockHeight'       => $peercoinBlockHeight ? $peercoinBlockHeight : 'N/A',
            'peercoinBlockTransactions' => $peercoinBlockTransactions ? $peercoinBlockTransactions : 'N/A',
            'peercoinPOSDifficutly'     => $peercoinPOSDifficutly ? $peercoinPOSDifficutly : 'N/A',
            'peercoinPrice'             => $peercoinPrice ? $peercoinPrice : 'N/A',
            'peercoinMint'              => $peercoinMint ? $peercoinMint : 'N/A',
            'peercoinStake'             => $peercoinStake ? $peercoinStake : 'N/A',
            'linuxCPUTemp'              => $linuxCPUTemp ? $linuxCPUTemp : 'N/A',
            'linuxSysLoad'              => $linuxSysLoad ? $linuxSysLoad : 'N/A',
            'linuxUsedMemory'           => $linuxUsedMemory ? $linuxUsedMemory : 'N/A',
            // 24h change
            'peercoin24hPercentChangeAvgCPULoad' => $linux24hPercentChangeAvhCPULoad ? $linux24hPercentChangeAvhCPULoad : 'N/A',
            'peercoin24hPercentChangeAvgUsedMemory' => $linux24hPercentChangeAvhUsedMemory ? $linux24hPercentChangeAvhUsedMemory : 'N/A',
            'peercoin24hPercentChangeAvgCPUTemp' => $linux24hPercentChangeAvhCPUTemp ? $linux24hPercentChangeAvhCPUTemp : 'N/A',
            'peercoin24hPercentChangeAvgConnections' => $peercoin24hPercentChangeAvgConnections ? $peercoin24hPercentChangeAvgConnections : 'N/A',
            'peercoin24hPercentChangeAvgTransactions' => $peercoin24hPercentChangeAvgTransactions ? $peercoin24hPercentChangeAvgTransactions : 'N/A',
            'peercoin24hPercentChangeAvgDifficulty' => $peercoin24hPercentChangeAvgDifficulty ? $peercoin24hPercentChangeAvgDifficulty : 'N/A',
            // peercointalk recent post
            'recentPostTitle'           => $recentPostTitle,
            'recentPostHref'            => $recentPostHref,
            // chart
            'chartPeriodHours'          => $chartPeriodHours,
            // connected peers
            'peercoinConnectedPeers'    => $peercoinConnectedPeers,
        ]);

    }

}