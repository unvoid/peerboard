<?php

namespace PeerboardBundle\Controller\Ajax;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PeerboardBundle\Library;
use PeerboardBundle\Controller\ExtendController;

/**
 * Class ChartController
 * @package PeerboardBundle\Controller
 */
class ChartController extends ExtendController
{

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request) {

        $temperatureRepo        = $this->em->getRepository('PeerboardBundle:DataTemperature');
        $connectionRepo         = $this->em->getRepository('PeerboardBundle:DataConnection');
        $CPULoadRepo            = $this->em->getRepository('PeerboardBundle:DataCPULoad');
        $POSDiffRepo            = $this->em->getRepository('PeerboardBundle:DataPOSDifficulty');
        $usedMemoryRepo         = $this->em->getRepository('PeerboardBundle:DataUsedMemory');
        $transactionsRepo       = $this->em->getRepository('PeerboardBundle:DataTransactions');

        /*
         * data for charts
         */

        $dataConnections = $connectionRepo->findLimit(1);
        $dataTemperature = $temperatureRepo->findLimit(1);
        $dataCPULoad = $CPULoadRepo->findLimit(1);
        $dataPOSDiff = $POSDiffRepo->findLimit(1);
        $dataUsedMemory = $usedMemoryRepo->findLimit(1);
        $dataTransactions = $transactionsRepo->getAvgTransactionsFromDateToDate(new \DateTime(),(new \DateTime())->modify('-1 minute'));

        $returnData = [
            'connections'   => $dataConnections[0]->getConnections(),
            'temperature'   => $dataTemperature[0]->getTemperature(),
            'cpuLoad'       => $dataCPULoad[0]->getCpuLoad(),
            'posDiff'       => $dataPOSDiff[0]->getDifficulty(),
            'usedMemory'    => $dataUsedMemory[0]->getUsedMemory(),
            'transactions'  => $dataTransactions ? $dataTransactions[0]->getTransactions() : 0,
            // labels
            'labelConnections' => $dataConnections[0]->getCreatedAt()->format('H:i'),
            'labelTemperature' => $dataTemperature[0]->getCreatedAt()->format('H:i'),
            'labelCpuLoad'  => $dataCPULoad[0]->getCreatedAt()->format('H:i'),
            'labelPosDiff'  => $dataPOSDiff[0]->getCreatedAt()->format('H:i'),
            'labelUsedMemory' => $dataUsedMemory[0]->getCreatedAt()->format('H:i'),
            'labelTransactions' => $dataTransactions ? $dataTransactions[0]->getCreatedAt()->format('H:i') : (new \DateTime)->format('H:i'),
        ];

        return new Response(json_encode($returnData), 200, ['Content-Type' => 'application/json']);

    }

}