<?php

namespace PeerboardBundle\Controller\Ajax;
use PeerboardBundle\Command\UpdateConnectedPeersCommand;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PeerboardBundle\Controller\ExtendController;


/**
 * Class PeerController
 * @package PeerboardBundle\Controller
 */
class PeerController extends ExtendController
{

    /**
     * @param Request $request
     * @return Response
     */
    public function newOldPeersAction(Request $request) {

        $returnData = [
            'peers' => [],
            'status' => 'error',
        ];

        if($this->cache->contains(UpdateConnectedPeersCommand::CACHE_KEY)) {

            $newOldPeers = $this->cache->fetch(UpdateConnectedPeersCommand::CACHE_KEY);

            $returnData['peers'] = unserialize($newOldPeers);
            $returnData['status'] = 'success';

        }

        return new Response(json_encode($returnData), 200, ['Content-Type' => 'application/json']);

    }

}